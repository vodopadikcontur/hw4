<?php
//Task 1
class Person {
    public $name;

    public $surname;

    public $age;

    public function __construct(string $name, int $age, string $surname = '')
    {
        $this->name = $name;
        $this->age = $age;
        $this->surname = $surname;
    }

    public function showFullInformation() {
        echo 'Name: ' .$this->name. ', Surname: ' .$this->surname. ', Age: ' .$this->age, '<br>';
    }
}

$person = new Person('Mike', 45, 'Korov');
$person->showFullInformation();

//Task 2
class Webpage {
    public string $title;

    public string $description;

    public string $keyword;

    public function __construct(string $title, string $keyword, string $description = '') {
        $this->cutTitle($title);
        $this->cutDesc($description);
        $this->cutKeyword($keyword);
    }

    public function cutTitle(string $title) {
        return $this->$title = mb_substr($title, 0, 80);
    }

    public function cutKeyword(string $keyword) {
        return $this->$keyword = mb_substr($keyword, 0, 120);
    }

    public function cutDesc(string $description) {
        return $this->$description= mb_substr($description, 0, 180);
    }

}

$webpage = new Webpage('title', 'desc', 'key');

//Task 3
$date = new DateTime('now');
echo $date->format('m'), '<br>';

echo 'Current year:' .$date->format('Y'). ', current month: ' .$date->format('m'). ', current day: ' .$date->format('d'), '<br>';

date_add($date, date_interval_create_from_date_string("5 days"));
echo $date->format('Y-m-d H:i:s'), '<br>';

date_sub($date, date_interval_create_from_date_string('5 days'));
echo $date->format('Y-m-d H:i:s'), '<br>';

$day = $date->format('d') - 1;
$date->sub(new DateInterval('P' .$day. 'D'));
echo $date->format('Y-m-d H:i:s'), '<br>';

$mounth = $date->format('m') - 1;
$date->sub(new DateInterval('P' .$mounth. 'M'));
echo $date->format('Y-m-d H:i:s');

